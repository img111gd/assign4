// Dale Garcia | IMG111 | Frank Niscak | ASSIGN4

// 4.1
// create a function that italicize all paragraphs in part1

function italicParagraphs(id) {
    var paragraphs = document.getElementsByClassName("part1");

    for (var counter = 0; counter < paragraphs.length; counter++) {
        var myParagraphs = paragraphs[counter];
        myParagraphs.style.fontStyle = "italic";
    }
}
italicParagraphs("part1");

// 4.2
// create a function that colors rows of the table in part2

function colorTableRows(id) {
    var mytable = document.getElementsByTagName('table');

    for (var line = 0; line < mytable[0].rows.length; line++);
        var myrow = mytable[0].rows[line];
        myrow.style.border = '2px solid red';

        for (var column = 0; column < myrow.cells.length ; column++)
            var mycell = myrow.cells[column];
            //mycell.style.backgroundColor = 'green';


}
colorTableRows('mytable');

// 4.3
// create a function that changes the table into a Bootstrap table

function makeBootstrapTable(id) {
    var newtable = document.getElementById('mytable').classList;
    mytable.table = mytable;
}
makeBootstrapTable('mytable');

// 4.4
// create a function that turns button01 and button04 red

function makeButtonTextRed(id) {
    var button = document.getElementById(id);
    button.style.color = " red ";
}
makeButtonTextRed("btn01");
makeButtonTextRed("btn04");


// 4.5
// create a funtion that will change button02 into

function makeButtonSmallRed(id) {
    var button = document.getElementById(id);
    button.style.color = "white";
    button.style.backgroundColor = "red";
    button.style.border = "0px";
}
makeButtonSmallRed("btn02");

// 4.6
// create a function that will remove button08

function removeButton(id) {
    var button = document.getElementById(id);
    button.removeChild("id");
}
removeButton("btn08");

// 4.7
// create a function that changes the appearance of the images in part3

function changeImagesInPart3() {
    var images = document.getElementsByTagName('img');

    for (var index = 0; index < images.length; index++) {
        var myImage = images[index];
        myImage.className += " rounded-circle";
        myImage.className += " rounded";
        myImage.className += " img-thumbnail";
    }
}
changeImagesInPart3('part3');

// 4.8
// create a function that makes border around items in UL in part4

function makeLiBorderInUl(id) {
    var li = document.getElementsByTagName('li');

    for (var index = 0; index < li.length; index++) {
        var myLi = li[index];

        myLi.style.border = "2px solid red";
    }
}
makeLiBorderInUl('ulist');

// 4.9
// create a function that appends additional elements to ordered list

function addLiInOlist(id, max_number) {
    var number = 0;
    for (var index = 0; index < 20; index++) {
        var node = document.createElement("LI");
        var textnode = document.createTextNode("New Fruit " + (number + 1));
        node.appendChild(textnode);
        document.getElementById("olist").appendChild(node);
    }
}
addLiInOlist('olist', 20);